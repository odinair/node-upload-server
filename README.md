# Node Upload Server

An extremely simple Node.js API server that allows abitrary file uploads to a specified folder.

This server makes absolutely no attempts to secure itself, as it's assumed you'll be running this behind some form of authentication
with a reverse proxy, such as using nginx with HTTP Basic authentication.

## Setup & Usage

### Server Side

This project is designed to work under a sub-directory on a domain, such as `i.example.com/upload`, while all uploaded files are served by
a web server that reverse proxies this server under that sub-directory.
([sample nginx config](https://gitlab.com/odinair/node-upload-server/blob/master/nginx.conf))

```bash
yarn

## Recommended environment variable setup:
# Setup the domain name that your uploaded files are served on
# This should end with a /, otherwise the given file URLs
# will be malformed
export HTTP_BASE=https://i.example.com
# Specify where your uploaded files are to be saved
export UPLOAD_DIR=/var/www/i.example.com/

node index.js
```

The following environment variables can be specified when running the server:

- `MAX_SIZE` - max uploadable file size **in bytes**; defaults to `20971520` (or about 20 MB)
- `UPLOAD_DIR` - the directory the server will store uploads in; defaults to `files` in the same directory that the `index.js` file resides in
- `PORT` - specifies the port the server should run on; defaults to `8000`
- `LISTEN_HOST` - the address for this server to listen on; defaults to `127.0.0.1` (localhost)
- `BASE_DIR` - the base directory the application should listen for requests on with sub-dir setups; defaults to `/` - **this should end with a `/`**
- `HTTP_BASE` - the base URI path to use when returning the uploaded file link; defaults to `/`
- `FRIENDLY_NAMES` - if this is set to any non-empty value, randomly generated file names will instead of being 8 random characters be a string of 3 random words, similarly to gfycat URLs

------

An additional server to render static markdown files is provided with `md-render.js`:

```bash
export PORT=8080
export LISTEN_HOST=127.0.0.1
export STATIC_DIR="$(pwd)/files"
node md-render.js
```

This markdown server accepts the following environment variables:

- `PORT` - specifies the port to listen on; defaults to `8080`
- `STATIC_DIR` - the base directory to read files from; defaults to `./files`
- `LISTEN_HOST` - the address for this server to listen on; defaults to `127.0.0.1` (localhost)

### Client Side

This server provides a very simple index page with a form to upload files with; however, the way this
server is designed to be used is by sending HTTP requests directly through httpie, curl, or anything
else capable of sending a `multipart/form-data` request.

The following example provides basic use cases with httpie, but really anything you can figure out
how to replicate this behaviour with should work.

```bash
$ # if you don't care about the file name and want the server to generate a random name for you,
$ # you can simply fire off a POST request to the server root
$ # the uploaded file must have the name 'file' in any given request, otherwise this will fail
$ # with a 400 Bad Request response
$ http --form --body POST https://i.example.com/upload file@my_file.txt
https://i.example.com/8DHvcpMq.txt
$ # the uploaded file is now at https://i.example.com/8DHvcpMq.txt

$ # or if you want to control the file name:
$ http --form --body PUT https://i.example.com/upload/supercoolimage.png file@image.png
https://i.example.com/supercoolimage.png
$ # if a file already exists with the given name, this will return a 409 Conflict status
$ # do note however that this doesn't support subdirectories, meaning a PUT request
$ # of `/directory/file` will fail, returning 404 Not Found.

$ # uploaded files can also be deleted at a later date without having to ssh/ftp in and
$ # manually delete them from the file system; if successful, this will return
$ # 204 No Content, otherwise 404
$ http DELETE https://i.example.com/upload/my_file_name.txt
```

Additionally, a `?redirect=1` parameter may also be provided when uploading files.
Any such requests will, instead of echoing back the uploaded file name, instead redirect to the
uploaded file. This is used when uploading files through the HTTP interface.
