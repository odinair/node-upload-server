server {
  ## setting up https is left as an excersise for the reader
  listen 80;
  server_name u.example.com;
  client_max_body_size 3M;
  gzip off;

  ## your upload server should be started with the `BASE_DIR` env variable set to `/upload`
  location /upload {
    ## recommended: require authentication to upload files
    # auth_basic "upload access is restricted";
    # auth_basic_user_file /tmp/htpasswd;
    ## if you configure this to be higher, remember to also set the `MAX_SIZE` environment variable
    ## when starting the server
    client_max_body_size 20M;
    proxy_pass http://127.0.0.1:8000;
  }

  ## optional: human-friendly markdown file rendering
  ## this requires `md-render.js` to also be running
  # location ~ ^(?!\/upload\/).*(\.(md|markdown)) {
  #   proxy_pass http://127.0.0.1:8080;
  # }

  location / {
    ## change this to wherever your upload server is configured to place files that it receives
    root /var/www/uploads;
    autoindex off;
  }
}
