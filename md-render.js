const express = require("express")
const path = require("path")
const fs = require("fs-extra")
const marked = require("marked")

const PORT = process.env.PORT || 8080
const STATIC_DIR = process.env.STATIC_DIR || path.join(__dirname, "files")

const app = express()
app.disable("x-powered-by")

function async_wrap(route) {
  return (req, res, next) => {route(req, res, next).catch(next)}
}

app.get("/*", async_wrap(async (req, res) => {
  let file = req.url.split("/").splice(1).join(path.sep)
  let p = path.join(STATIC_DIR, file)
  if(!await fs.exists(p)) {
    console.log(`File '${p}' does not exist, sending 404`)
    return res.sendStatus(404)
  }
  marked((await fs.readFile(p)).toString("utf8"), (err, rendered) => {
    console.log(`Rendering file '${p}'`)
    if(err) return next(err)
    res.header('Content-Type', 'text/html')
    res.send(rendered)
  })
}))

app.use((err, req, res, next) => {
  console.error(`Request to ${req.url} failed with method ${req.method}`)
  console.error(err)
  res.sendStatus(500)
})

app.listen(PORT, process.env.LISTEN_HOST || "127.0.0.1", () => {
  console.log(`Listening on port ${PORT}`)
})
